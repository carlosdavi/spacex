/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import React from 'react'
import {Provider} from 'react-redux'

import storeconfig from './src/store/storeConfig'

const store = storeconfig()
const Redux = () => 
    <Provider store={store}>
        <App />
    </Provider>

//AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerComponent(appName, () => App);
