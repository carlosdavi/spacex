import React,{Component} from "react";
import { View, Text } from "react-native";
import { createStackNavigator,createAppContainer } from "react-navigation";
import Principal from './src/componentes/Principal'
import HomeScreen from './src/componentes/HomeScreen'
import Article from './src/componentes/Article'
import DetalhesMissao from './src/componentes/DetalhesMissao'

const AppNavigator = createStackNavigator({
  
  Home: Principal,
  HomeScreen:HomeScreen,
  DetalhesMissao: DetalhesMissao,
  Article: Article,
  },
  {
    initialRouteName: 'HomeScreen'
  }
);

const AppContainer  = createAppContainer(AppNavigator);

export default class App extends Component {
  render() {
    return (
      <AppContainer  />
    );
  }
}

