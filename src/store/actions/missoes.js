import { FILTRAR_MISSOES , RESTAURAR_MISSOES, CARREGAR_MISSOES } from './actionsTypes'

export const carregar = listaMissoes => {
    return {
        type: CARREGAR_MISSOES,
        payload: listaMissoes
    }
}

export const filtrar = listaMissoes => {
    return {
        type: FILTRAR_MISSOES,
        payload: listaMissoes
    }
}

export const restaurar = () => {
    return {
        type: RESTAURAR_MISSOES
    }
}