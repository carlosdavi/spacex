import {USER_LOGGED_IN, USER_LOGGED_OUT} from './actionsTypes'

export const login = user => {
    return{
        type: USER_LOGGED_IN,
        payload: user
    }
}