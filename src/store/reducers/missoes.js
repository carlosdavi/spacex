import { FILTRAR_MISSOES , RESTAURAR_MISSOES, CARREGAR_MISSOES } from '../actions/actionsTypes'
import { combineReducers } from 'redux'

const initialState = {
    missoesFull    : null,
    missoesDisplay : null,
    loadedRequest  : false

}

const reducers = (state = initialState, action) => {
    switch (action.type) {
        case CARREGAR_MISSOES:
            return {
                ...state,
                missoesFull : action.payload.data,
                missoesDisplay : action.payload.data,
                loadedRequest : true
            }
        case FILTRAR_MISSOES:
            return {
                ...state,
                missoesDisplay : action.payload.data
            }
        case RESTAURAR_MISSOES:
            return {
                ...state,
                missoesDisplay : missoesFull
            }  
        default:
            return state          
    }
}
 export default reducers
 //export default combineReducers({
  //   missoesR: reducers
 //})