import {createStore, combineReducers } from 'redux'
import missoesReducer from './reducers/missoes'

const reducers = combineReducers({
    missoes: missoesReducer // chave global com dados das missoes
})

const storeConfig = () => {
    return createStore(reducers)
}

export default storeConfig