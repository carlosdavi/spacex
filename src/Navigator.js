import React from 'react'
import {createBottomTabNavigator} from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome'
import Inicio from '../src/componentes/Principal'
import VideoInicio from '../src/componentes/VideoInicio'
import Article from '../src/componentes/Article'

const MenuRoutes = {
    Fotos: {
        name: 'Fotos',
        screen: Inicio,
        navigationOptions:{
            title: 'Fotos',
            tabBarIcon: ({ tintColor }) =>
            <Icon name='camera' size={30} color={tintColor} />
        }
    },
    Principal: {
        name: 'SpaceX',
        screen: Inicio,
        navigationOptions:{
            title: 'Inicio',
            tabBarIcon: ({ tintColor }) =>
            <Icon name='home' size={30} color={tintColor} />
        }
    },
    Artigo: {
        name: 'Artigo',
        screen: Article,
        navigationOptions:{
            title: 'Artigo Space X',
            tabBarIcon: ({ tintColor }) =>
            <Icon name='mobile' size={30} color={tintColor} />
        }
    },
   Videos: {
        name: 'Video',
        screen: VideoInicio,
        navigationOptions:{
            title: 'Fotos',
            tabBarIcon: ({ tintColor }) =>
            <Icon name='users' size={30} color={tintColor} />
        }
    }

}
const MenuConfig = {
    initialRouteName: 'Principal',
    tabBarOptions:{
        showLabel: false,
    }       
}
const MenuNavigator = createBottomTabNavigator(MenuRoutes,MenuConfig)
export default MenuNavigator