import React,{Component} from 'react'
import {StyleSheet,Text,View,Platform,Image} from 'react-native'
import icon from '../../assets/imgs/spacex_logo_white.png'

class Header extends Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.rowContainer}>   
                    <Image source={icon} style={styles.image} />                    
                </View>     
            </View>
        ) 
    }

}

const styles = StyleSheet.create({
    container:{
        marginTop: 20,
        padding: 10,
        borderBottomWidth: 1,
        borderColor: '#212121',
        backgroundColor:'#212121',
        alignItems: 'center'
    },
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        height: 30,
        width:100,
        resizeMode: 'contain'          
    }
})
export default Header