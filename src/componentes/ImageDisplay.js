import React, {Component} from 'react'
import {View,Image,Dimensions} from 'react-native'
//import urls from '../dados/urls'
urls = require('../dados/urls')

class ImageDisplay extends Component{
    render(){
        const { width } = Dimensions.get('window')
        return(
        <View>
           <Image
                style={{width , height: 300}}
                source={{uri: urls.urlImagem()}}
                />
           </View>
        )

    }
}

export default ImageDisplay