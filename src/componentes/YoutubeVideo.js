import React,{Component} from 'react'
import { View} from 'react-native'
import YouTube  from 'react-native-youtube'

class YoutubeVideo extends Component {
    state = {
        fullScreen: false,
        play : true
    }
    render(){
        const id_video = this.props.id_video
        
    return(
        <View>        
                <YouTube
                    videoId={id_video}   
                    play={this.state.play}             
                    fullscreen={this.state.fullScreen}       
                    loop={true}                     
                    apiKey="AIzaSyAePJJhLZu7m6JvY6XiX4kC2ShlljNHkU0"
                    style={{ alignSelf: 'stretch', height: 200 }}
                /> 
        </View>
        )
    }
}

export default YoutubeVideo    