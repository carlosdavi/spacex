import React,{Component} from 'react'
import {Text,View,StyleSheet,Image,Dimensions,Button,Alert,TouchableHighlight} from 'react-native'
urls = require('../dados/urls')

class elementoLista extends Component {
    render(){
        const { width } = Dimensions.get('window')
        
        return(
           
            <View style={styles.container}>
            <Text style={styles.missao} >Missão: {this.props.elemento.mission_name}</Text>
            </View>
     
        )
    }
}


const styles = StyleSheet.create({
    
    container: {
        backgroundColor:'#212121',
        paddingHorizontal:10,
        marginVertical:5,
        marginHorizontal:5,
        borderRadius:10,
        borderWidth:2,
        borderColor:'#BDBDBD',
        fontSize:24,
        fontWeight:'bold'
    },
    missao : {
        marginTop:20,
        textAlign:'center',
        fontSize:20,
        color:'#FFFFFF'
    },
    rocket_name: {
        textAlign:'center',
        fontSize:18
    },
    imagem: {        
        //width: Dimensions.get('window').width,
        height: 100,
        flex:1,
        aspectRatio:1.5,
        resizeMode:'contain'
    }


})

export default elementoLista