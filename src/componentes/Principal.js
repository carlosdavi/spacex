import React,{Component} from 'react';
import { StyleSheet, Text, View,ScrollView } from 'react-native';
import Header from './Header'
import ImageDisplay from './ImageDisplay'
import SpaceXApi from './spacexApi'

export default class Principal extends Component {
 render (){
    return(
      <View>
        
        <Header/>        
        <ScrollView>
        <ImageDisplay />
        <SpaceXApi navigation={this.props.navigation}/>
        
          
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
