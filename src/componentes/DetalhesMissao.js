import React,{Component} from 'react'
import {View, Text,StyleSheet,Image,Dimensions,TouchableHighlight} from 'react-native'
import YoutubeVideo from './YoutubeVideo'
class DetalhesMissao extends Component {
    
    static navigationOptions = ({navigation}) => {return{title: navigation.getParam('elemento',' ').mission_name}}
    state = {
        fullScreen : true   
     }
     
     navegar = (item) => {this.props.navigation.navigate('Article', {artigo:item} ) }

    render(){
        item = this.props.navigation.getParam('elemento','Sem valores')
        const { width } = Dimensions.get('window')
        return(
            <View style={styles.container}>
                <Text style={styles.title}> Informações</Text>
                <Text style={styles.text}> Missão: {item.mission_name}</Text>
                <Text style={styles.text}> Rocket: {item.rocket.rocket_name}</Text>
                <Text style={styles.text}>Ano: {item.launch_year}</Text>
                <Text style={styles.text}>Data UTC: {item.launch_date_utc}</Text>
                <Text style={styles.texto_video}> Vídeo do Lançamento </Text>
                <YoutubeVideo style={styles.video} id_video={item.links.youtube_id}/> 
                        
                <TouchableHighlight onPress={() => this.navegar(item)}>
                    <Text style={styles.botao}>Artigo</Text>
                </TouchableHighlight>
                <Image
                    style={styles.image}
                    source={{uri: item.links.mission_patch_small}}
                />
            </View>
        )
    }
}

const { width } = Dimensions.get('window')
    styles = StyleSheet.create({
        container: {
            backgroundColor: '#212121'
        },
        
        title: {
            paddingVertical:10,
            fontSize:24,
            fontWeight:'bold',
            color: '#FFFFFF'
        },
        text:{
            color: '#FFFFFF'
        },
        item:{
            fontSize:18
        },
        image: {
           height: 100,
           aspectRatio: 1.5,
           resizeMode: 'contain',
           alignSelf: 'center'
        },
        player: {
            alignSelf: 'stretch',
            marginVertical: 10,
        },
        texto_video:{
            paddingVertical:15,
            marginVertical:15,
            textAlign:'center',
            fontSize:18,
            color: '#FFFFFF',
            fontWeight:'bold'
        },
        video:{
        paddingVertical:5,
        marginVertical:5,
        marginHorizontal:5,
        borderRadius:10,
        borderWidth:2,
        borderColor:'#BDBDBD'
        },
        botao: {
            backgroundColor:'#212121',
            textAlign:'center',
            paddingHorizontal:10,
            marginVertical:5,
            marginHorizontal:5,
            borderRadius:10,
            borderWidth:2,
            borderColor:'#BDBDBD',
            fontSize:24,
            fontWeight:'bold',
            color: '#FFFFFF'
        }
        
    })

export default DetalhesMissao