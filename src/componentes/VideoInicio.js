import React,{Component} from 'react'
import { StyleSheet, Text, View, Dimensions, WebView } from 'react-native'
import { Video } from 'expo'
//import YouTube from '../../node_modules/react-native-youtube'
import Header from './Header'
 url = require( '../dados/urls')

/*class VideoInicio extends Component{
  render(){
    return (
    <View>
        <YouTube
          videoId="AQFhX5TvP0M"   // The YouTube video ID
          play={true}             // control playback of video with true/false
          fullscreen={true}       // control whether the video should play in fullscreen or inline
          loop={true}             // control whether the video should loop when ended

          onReady={e => this.setState({ isReady: true })}
          onChangeState={e => this.setState({ status: e.state })}
          onChangeQuality={e => this.setState({ quality: e.quality })}
          onError={e => this.setState({ error: e.error })}

          style={{ alignSelf: 'stretch', height: 300 }}
        />
      </View>
      )
    }
}*/

 /*class VideoInicio extends React.Component {
    render() {
      const { width } = Dimensions.get('window');
        
      return (
        <View>
          <Header />
          <View style={styles.container}>
          
         {/* <Video
            source={{ uri: url.urlI() }}
                shouldPlay
            resizeMode="cover"
            style={{ width, height: 300 }}
          /> }
          </View>
          <WebView
            style={{flex:1}}
            javaScriptEnabled={true}
            source={{uri: 'https://www.youtube.com/watch?v=AQFhX5TvP0M'}}
          />
        </View>
      );
    }
 } */

 class VideoInicio extends Component {
  render() {
    return (
      <WebView
        source={{uri: url.urlVideo()}}
        style={{marginTop: 20,height:200}}
      />
      
    );
  }
}
  
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    controlBar: {
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
      height: 45,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: "rgba(0, 0, 0, 0.5)",
    }
  });

  export default VideoInicio