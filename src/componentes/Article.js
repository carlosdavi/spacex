import React,{Component} from 'react'
import {WebView,StyleSheet} from 'react-native'

class Article extends Component{
    static navigationOptions = ({navigation}) => {return{title: 'Artigo'}}

    render(){
        const item = this.props.navigation.getParam('artigo','Sem valores').links.article_link
        
        return(
            <WebView
            source={{uri: item}}
            style={{marginTop: 20,height:200}}
          />
        );
}
}
styles = StyleSheet.create({
    video:{
        marginTop: 20,
        height:200
    }
})
export default Article