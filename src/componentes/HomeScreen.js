import React from 'react';
import { StyleSheet, Text,TouchableHighlight,FlatList,Image,TextInput,View} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { bindActioncreators } from 'redux'
import { connect }  from 'react-redux'
import { carregar, filtrar, restaurar } from '../store/actions/missoes'
import restApi from './restApi'
import ElementoLista from './elementoLista'
const logo = require('../../assets/imgs/spacex_logo_white.png')



class HomeScreen extends React.Component {
  state = {
    itens: [],
    dataSource: [],
    text: '',
    loadedRequest : false
}
arrayholder = []
  static navigationOptions = {
    //title: 'SPACEX'   
    headerTitleStyle:{
      color:'#fff',
      flex: 1,
      textAlign: 'center'
    },    
    headerStyle: {
      backgroundColor:'#212121'
    },
    headerTitle: (<View style={{ flex: 1, alignItems: "center" }}>
      <Image source={logo} />
      </View>),
      headerLeft: null, 
      headerRight: null    
  };

    alterarLista = (listaModificada) => {this.setState({itens: {...listaModificada}})}
  
    navegar = (item) => {this.props.navigation.navigate('DetalhesMissao', {elemento:item} ) }
    setarDados = (response) => {  
      if(this.state.loadedRequest == false && response != null){
        this.setState({itens :{...response},dataSource: {...response}})
        this.setState({loadedRequest : true})
       // this.props.onMissoes({...this.state})
      }
    }
    
    onSearchChange = (filtro) => {
      if (filtro.length > 1){
          itensFiltrar = this.state.itens.data
          var filtered = { 
          data: itensFiltrar.filter((item) => {
          const itemUpperCase = item.mission_name.toUpperCase()
          const filtroUpperCase  = filtro.toUpperCase() 
          return itemUpperCase.indexOf(filtroUpperCase) > -1}) }
          this.alterarLista(filtered)
        } 
      else { this.setState({itens: {...this.state.dataSource }}) } 
    }

    renderItem = (item) =>
        <TouchableHighlight onPress={() => this.navegar(item)}>
           <ElementoLista elemento={item} navigation={this.props.navigation } />       
        </TouchableHighlight>
       
    render() {
      const {missoes} = this.props
      restApi.get(this.setarDados)
     
    return (
      <View>
        <TextInput
          style={styles.textInput}
            placeholder="Buscar Missão"
            onChangeText={text => this.onSearchChange(text)}
        />
        <ScrollView style={styles.container}>  
          <FlatList
            data={this.state.itens.data}
            keyExtractor={(item) => item.flight_number.toString()}
            renderItem={({item}) => this.renderItem(item)}
          />
        </ScrollView>
      </View>
    )
}
}
styles = StyleSheet.create({
  container: {
    backgroundColor:'#212121'    
  },
  texts: {
    color:'#fff'
  },
  textInput:{
    textAlign: 'center',
    height: 20,
    borderWidth: 1,
    borderColor: '#009688',
    marginHorizontal:5,
    borderRadius:10,
    borderWidth:2,
    }
})


const mapDispatchToProps = dispatch => (
  bindActioncreators ({carregar,filtrar,restaurar},dispatch)
  )

  const mapStateToProps = (state) => {
  const listamissoes = {...state}
  return listamissoes
}

export default HomeScreen
//export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);